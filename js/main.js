var canvas = document.getElementById("gameField");
canvas.width = 800;
canvas.height = 600;
var ctx = canvas.getContext("2d");
var amplitude = canvas.height / 2;
var frequency = 0.01;
var phi = 0;
var frames = 0;
var heightCoefficient = 2;
var minHeightCoefficient = 1.46;
var maxHeightCoefficient = 5.96;
ctx.lineWidth = 1.1;

var circle = 
{
    x: 0,
    y: 0,
    health: 3,
    circleOffset: 627,
    circleRadius: 10,
    circleBackground: "#fff",
    draw: function ()
    {
        ctx.beginPath();
        ctx.arc(this.x - this.circleOffset, this.y, this.circleRadius, 0.01 * Math.PI, false);
        ctx.fillStyle = this.circleBackground;
        ctx.fill();
        ctx.stroke();
    }
}

window.requestAnimationFrame(gameLoop);