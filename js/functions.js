function changeWaveHeight(e) 
{
	var downButtonKeyCode = 40;
	var upButtonKeyCode = 38;
    if (e.keyCode == downButtonKeyCode) 
    {
  	    window.heightCoefficient += 0.06;
        if (window.heightCoefficient >= window.maxHeightCoefficient)
        {
            window.heightCoefficient = window.maxHeightCoefficient;
        }
    }
    if (e.keyCode == upButtonKeyCode) 
    {
  	    window.heightCoefficient -= 0.06;
        if (window.heightCoefficient <= window.minHeightCoefficient)
        {
            window.heightCoefficient = window.minHeightCoefficient;
        }
    }
    console.log(heightCoefficient);
}

function drawGameInfo()
{
    ctx.fillStyle = "#000";
    ctx.font = "20px Arial";
    ctx.fillText("Press ↑ to increase line height", 10, 35);
    ctx.fillText("Press ↓ to decrease line height ", 10, 58);
}

function drawLineAndCircle()
{
    //draw Line
	ctx.beginPath();
	for (var x = 0; x < canvas.width; x++) 
    {
        y = Math.sin(x * frequency + phi) * amplitude / heightCoefficient + amplitude;
        ctx.lineTo(x, y);
    }
    ctx.stroke();

    //draw Circle
    circle.x = x;
    circle.y = y;
    circle.draw();
}

function cleanGameField()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function gameLoop() 
{
    frames++;
    phi = frames / 30;
    document.addEventListener('keydown', changeWaveHeight, false);
    cleanGameField();
    drawGameInfo();
    drawLineAndCircle();

    window.requestAnimationFrame(gameLoop);
}